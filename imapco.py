#!/usr/bin/env python3
# pylint: disable=too-many-locals,too-many-branches,too-many-statements
# "ns" for namespace is perfectly reasonable, so - pylint: disable=invalid-name
""" IMAP copy but in Python.
"""

from contextlib import contextmanager
from datetime import datetime as dt
from imaplib import IMAP4, IMAP4_SSL
import logging
import os
from pathlib import Path
import re
import time

import click
import tomli


log = logging.getLogger()


CONFIG_FILE = Path(os.environ["HOME"]) / Path(".imapco.py.ini")

NS_PERSONAL = 0
NS_SHARED = 1
NS_PUBLIC = 2

FLAGS = [
    "\\Seen", "\\Answered", "\\Flagged", "\\Deleted", "\\Draft", "\\Recent",
    "$Forwarded", "$MDNSent", "$Junk", "$NotJunk", "$Phishing",
]


@contextmanager
def dots():
    """ Progress dots. """
    def _dots():
        while True:
            for i, char in enumerate("⠁⠃⠇⡇⣇⣧⣷⣿"):
                if i:
                    yield f"\x08{char}"
                else:
                    yield char
    dotty = _dots()
    def step():
        print(next(dotty), end="", flush=True)
    try:
        # hide cursor
        print("\x1b[?25l", end="", flush=True)
        yield step
    finally:
        print("\x1b[?25h", flush=True)


def parse_namespace(ns):  # pylint: disable=invalid-name
    """ Parse one RFC2342 namespace. """
    if ns == "NIL":
        return None
    prefix, separator = re.match(r"\(\(\"([^\"]*)\" \"([^\"]*)\"\)\)", ns).groups()
    return prefix, separator


def get_namespaces(server):
    """ Retrieve RFC2342 personal, shared, and public namespace from server. """
    code, namespaces = server.namespace()
    assert code == "OK", f"failed to get namespaces from {server.host}"
    personal_ns, shared_ns, public_ns = re.match(
        r"(\(\(.*\)\)|NIL) (\(\(.*\)\)|NIL) (\(\(.*\)\)|NIL)",
        namespaces[0].decode("utf-8")
    ).groups()
    personal_ns = parse_namespace(personal_ns)
    shared_ns = parse_namespace(shared_ns)
    public_ns = parse_namespace(public_ns)
    return personal_ns, shared_ns, public_ns  # order: NS_PERSONAL, NS_SHARED, NS_PUBLIC


def get_server(server_config):
    """ Get IMAP4 server from config. """
    host = server_config["host"]
    port = server_config["port"]
    if port == 993:
        server = IMAP4_SSL(host, 993)
    else:
        server = IMAP4(host, port)
    status, _ = server.noop()
    assert status == "OK", f"failed to connect to {host}:{port}"
    status, _ = server.login(server_config["user"], server_config["pass"])
    assert status == "OK", f"failed to log into {host}:{port}"
    return server


@click.group()
def cli():
    """IMAP server-server copy tool.
    """


class Mailbox:
    """ Mailbox on an IMAP4 server. """

    @staticmethod
    def get_mailboxes(server):
        """ Get list of mailbox names on server.
            These names can them be used to instantiate `Mailbox` objects.
        """
        status, raw_folders = server.list("*")
        assert status == "OK", f"cannot get mailbox list from {server.host}"
        mailboxes = []
        for raw in raw_folders:
            # raw format: b'(\\HasNoChildren \\Drafts) "." INBOX.INBOX.Drafts'
            _flags, _sep, name = re.match(
                r"^\(([^\)]*)\) \"([^\"]*)\" (.*)$",
                raw.decode("utf-8"),
            ).groups()
            mailboxes.append(name)
        return mailboxes

    def __init__(self, server, name, *, subscribe=False, create=False, readonly=True, debug=None):
        """ Mailbox on an IMAP4 server.

            - subscribe (bool) to subscribe to mailbox
            - create (bool) to try and create mailbox before use
            - readonly (bool) to protect mailbox against appending
            - debug (callable) debug messages are passed to this function, e.g. `print`
        """
        self.server = server
        self.name = name
        self.readonly = readonly
        self.debug = debug
        if create:
            status, resp = server.create(name)
            # response if mailbox already exists:
            # NO [b'[ALREADYEXISTS] Mailbox already exists (0.001 + 0.000 secs).']
            if status != "OK" and "ALREADYEXISTS" not in resp[0].decode("utf-8"):
                raise ValueError(
                        f"{server.host}:{name} {status} cannot create mailbox: {resp}"
                )
        if subscribe:
            status, resp = server.subscribe(name)
            if status != "OK":
                raise ValueError(
                    f"{server.host}:{name} {status} cannot subscribe to mailbox: {resp}"
                )
        status, resp = server.select(name, readonly=readonly)
        if status != "OK":
            if self.debug:
                self.debug(f"{server.host}:{name} {status} {resp}, trying to subscribe first")
            server.subscribe(name)
            status, resp = server.select(name, readonly=readonly)
        if status != "OK":
            raise ValueError(
                f"{server.host}:{name} {status} cannot open mailbox: {resp}"
            )
        self.n_messages = int(resp[0].decode("utf-8"))

    def __str__(self):
        return f"{self.server.host}:{self.name} ({self.n_messages} messages)"

    def get_messages(self, max_messages=None):
        """ Get up to max_messages messages from mailbox. """
        if max_messages is None:
            max_messages = self.n_messages
        max_messages = min(max_messages, self.n_messages)
        for msg_number in range(1, max_messages+1):
            yield self.get_message(msg_number)

    def get_message(self, msg_number):
        """ Fetch message from server. """
        msg_number = str(msg_number)
        status, resp = self.server.fetch(msg_number, "(FLAGS INTERNALDATE BODY[])")
        if status != "OK":
            raise ValueError(
                f"{self.server.host}:{self.name} {status} failed to fetch #{msg_number}: {resp}"
            )
        msg, body = resp[0]
        assert resp[1] == b")"
        msg_match = re.match(
            r"(\d+) \(FLAGS \(([^\)]*)\) INTERNALDATE \"([^\"]+)\" BODY\[\] \{(\d+)\}",
            msg.decode("utf8"),
        )
        if not msg_match:
            raise ValueError(
                f"{self.server.host}:{self.name} invalid message pattern #{msg_number}: {msg}"
            )
        msg_number_check, flags, internal_date, body_size = msg_match.groups()
        if msg_number != msg_number_check:
            raise ValueError(
                f"{self.server.host}:{self.name} invalid message number for #{msg_number}: {msg}"
            )
        if int(body_size) != len(body):
            raise ValueError(
                f"{self.server.host}:{self.name} invalid message size for #{msg_number}: {msg}"
            )
        flags = flags.split(" ")
        # date format is weird, contains month by name: "21-Apr-2013 22:29:25 +0200"
        internal_date = dt.strptime(internal_date, "%d-%b-%Y %H:%M:%S %z")
        return internal_date, flags, body

    def append_message(self, internal_date, flags, msg_body):
        """ Append message to mailbox. """
        if self.readonly:
            raise ValueError(
                f"{self.server.host}:{self.name} cannot append to read-only mailbox"
            )
        status, resp = self.server.append(
            self.name,
            " ".join(flags),
            internal_date,
            msg_body,
        )
        if status != "OK":
            raise ValueError(
                f"{self.server.host}:{self.name} {status} cannot append message: {resp}"
            )


class DryrunMailbox:
    """ Dryrun Mailbox on an IMAP4 server. """

    def __init__(self):
        pass

    def __str__(self):
        return "dryrun mailbox"

    def append_message(self, internal_date, flags, msg_body):
        """ Pretend to append message to mailbox. """


def get_target_folder(mailbox, *, source_config, target_config):
    """ Get matching target folder for source mailbox.
        This will fix differences in separators, fix duplicate INBOX.INBOX
        naming, and add the optional target root folder.
        From `config`, source and target namespace info is used, as
        well as the target `root_folder`.
    """
    target_folder, target_sep = target_config["namespaces"][NS_PERSONAL]
    if target_config.get("root_folder"):
        if target_folder and not target_folder.endswith(target_sep):
            target_folder += target_sep
        target_folder += target_config["root_folder"]
    source_ns, source_sep = source_config["namespaces"][NS_PERSONAL]
    if mailbox not in ("INBOX", source_ns):
        if source_ns and not source_ns.startswith(source_sep):
            source_ns += source_sep
        if source_ns and mailbox.startswith(source_ns):
            stem = mailbox[len(source_ns):]
        else:
            stem = mailbox
    else:
        stem = mailbox
    stem = stem.replace(source_sep, target_sep)  # b/c stem will added to target
    if target_folder and not target_folder.endswith(target_sep):
        target_folder += target_sep
    target_folder += stem
    single_inbox = "INBOX" + target_sep
    double_inbox = "INBOX" + target_sep + "INBOX" + target_sep
    target_folder = target_folder.replace(double_inbox, single_inbox)
    return target_folder


def check_mailboxes(source_config, mailboxes):
    """ Filter out mailboxes not suitable (or wanted) for copying. """

    def in_namespace(mailbox, namespace):
        if not namespace:
            return False
        ns, sep = namespace
        if mailbox == ns or mailbox.startswith(ns + sep):
            return True
        return False

    for mailbox in mailboxes:
        if source_config.get("folders") and mailbox not in source_config["folders"]:
            print(f"Skipping non-whitelisted mailbox {mailbox}")
            continue
        if source_config.get("skip") and mailbox in source_config["skip"]:
            print(f"Skipping blacklisted mailbox {mailbox}")
            continue
        if in_namespace(mailbox, source_config["namespaces"][NS_SHARED]):
            print(f"Skipping shared mailbox {mailbox}")
            continue
        if in_namespace(mailbox, source_config["namespaces"][NS_PUBLIC]):
            print(f"Skipping public mailbox {mailbox}")
            continue
        yield mailbox


@cli.command()
@click.option(
    "--max-messages", "-n",
    default=None,
    help="copy only first N message of each folder",
)
@click.option(
    "--dryrun",
    is_flag=True, default=False,
    help="don't actually copy any messages, nor create any folders",
)
@click.option(
    "--create-empty",
    is_flag=True, default=False,
    help="create empty folders on target",
)
@click.option(
    "--all-to-inbox",
    is_flag=True, default=False,
    help="""copy all messages to INBOX, don't create folders on target.
    (!)WARNING(!) This will clutter up your TARGET inbox.""",
)
@click.option(
    "--separator",
    default=",",
    help="separator for individual mailboxes in SOURCE argument",
)
@click.option(
    "--debug",
    is_flag=True,
    default=False,
    help="more verbose output",
)
@click.argument("source", required=True)
@click.argument("target", required=True)
def copy(
    max_messages, create_empty, all_to_inbox,
    dryrun, debug, separator, source, target,
):  # pylint: disable=too-many-arguments
    """Copy messages from one IMAP server to another.

    SOURCE and TARGET refer to entries in the config file .imapco.py.ini

    SOURCE may contain a list of folders to copy, otherwise all source folders
    (that aren't blacklisted) are copied. Those folders are separated by a comma
    (can be changed with --separator). Example:

        old_server:Inbox,Outbox,Archive

    TARGET may contain an optional root folder, thus overriding any root folder
    from the target's config. Example:

        new_server:OldServerArchive

    Full examples:

    Dryrun of a migration:

        imapco.py copy --dryrun old_server:Inbox,Sent,Archive new_server:Migration

    Full migration:

        imapco.py copy --create-empty old_server new_server

    Recreate folder structure of old server on new server, under configured
    root folder (if any):

        imapco.py copy --create-empty --max-messages 0 old_server new_server
    """
    start = time.monotonic()
    config = tomli.loads(open(CONFIG_FILE).read())
    if debug:
        config["debug"] = True
        debug = print
    else:
        debug = lambda _: None
    if max_messages is not None:
        max_messages = int(max_messages)
    source, _, source_folders = source.partition(":")
    if source not in config:
        print(f"unknown source configuration: {source}")
        return 1
    if source_folders:
        config[source]["folders"] = source_folders.split(separator)
        debug(f"source folders: {config[source]['folders']}")
    target, _, target_root = target.partition(":")
    if target not in config:
        print(f"unknown target configuration: {target}")
        return 1
    if target_root:
        config[target]["root_folder"] = target_root
        debug(f"target root: {config[target]['root_folder']}")
    total_messages = 0
    total_folders_created = 0
    total_folders_skipped = 0
    with get_server(config[source]) as source_server, \
         get_server(config[target]) as target_server:
        config[source]["server"] = source_server
        config[source]["namespaces"] = get_namespaces(source_server)
        config[target]["server"] = target_server
        config[target]["namespaces"] = get_namespaces(target_server)
        mailboxes = Mailbox.get_mailboxes(source_server)
        for mailbox in check_mailboxes(config[source], mailboxes):
            debug(f"Copying mailbox {mailbox}")
            source_mailbox = Mailbox(
                source_server,
                mailbox,
                readonly=True,
                debug=debug,
            )
            n_messages = min(
                max_messages or source_mailbox.n_messages,
                source_mailbox.n_messages,
            )
            debug(source_mailbox)
            if n_messages == 0 and not create_empty:
                print(f"Skipping empty mailbox {mailbox}")
                continue
            if all_to_inbox:
                target_folder = "INBOX"
            else:
                target_folder = get_target_folder(
                    mailbox,
                    source_config=config[source],
                    target_config=config[target],
                )
            total_folders_created += 1
            debug(f"Target for {mailbox} is {target_folder}")
            if not dryrun:
                target_mailbox = Mailbox(
                    target_server,
                    target_folder,
                    create=True,
                    readonly=False,
                    debug=debug,
                )
            else:
                target_mailbox = DryrunMailbox()
            print(
                f"Copying {n_messages} messages from {mailbox} to {target_folder} ",
                sep="", end="", flush=True,
            )
            with dots() as dot:
                for internal_date, flags, msg_body in source_mailbox.get_messages(n_messages):
                    flags = [
                        flag for flag in flags
                        if flag in config[target].get("supported_flags", FLAGS)
                    ]
                    target_mailbox.append_message(internal_date, flags, msg_body)
                    total_messages += 1
                    dot()
        total_folders_skipped = len(mailboxes) - total_folders_created
        print("Statistics:")
        print(f"{len(mailboxes):<6} mailboxes on {config[source]['host']}")
        print(f"{total_folders_created:<6} folders created on {config[target]['host']}")
        print(f"{total_folders_skipped:<6} folders skipped")
        print(f"{total_messages:<6} messages copied")
        print(f"Took {int(time.monotonic()-start)}s")
    return 0


@cli.command()
@click.argument("servers", nargs=-1)
def info(servers):
    """ Show info about one or more IMAP4 servers (or all, if omitted).
    """
    config = tomli.loads(open(CONFIG_FILE).read())
    if not servers:
        servers = sorted(config.keys())
    for server in servers:
        if server not in config:
            print(f"no entry for {server}")
            return 1
        print(f"server {server}")
        with get_server(config[server]) as srv:
            print(f"host: {srv.host}:{srv.port}")
            print("welcome:", srv.welcome.decode("utf-8"))
            print("capabilities:", sorted(srv.capabilities))
            print("UTF-8 enabled:", srv.utf8_enabled)
            print("namespaces:")
            for ns, nsinfo in zip(("personal", "shared", "public"), get_namespaces(srv)):
                if nsinfo:
                    nsinfo = f"prefix: {nsinfo[0]!r}, separator: {nsinfo[1]!r}"
                print(f"  - {ns}: {nsinfo}")
        print("")
    return 0


if __name__ == "__main__":
    cli()  # pylint: disable=no-value-for-parameter
