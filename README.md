# IMAP COpy in PYthon

Copy messages from one IMAP account to another.

Configuration file `~/.imapco.py.ini`:

    [source]
    host = imap.example.com
    port = 993
    user = foo@example.com
    pass = xxx123
    subscribe = false
    skip = ["INBOX.Trash"]
    folders = ["A", "B", "C"]

    [target]
    host = imap.example.com
    port = 993
    user = bar@example.com
    pass = yyy456
    subscribe = false
    supported_flags = ["\\Recent", "\\Flagged"]
    root_folder = Migration

Invocation may override (source) folders and (target's) root folder:

    imapco.py -n 1 --sep=, mailbox_org:A,B,C migadu:migration-folder




# Reading List

https://www.rfc-editor.org/rfc/rfc9051

